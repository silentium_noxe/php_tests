<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 12:19
 */
namespace com\bysilentium_noxe\php\tests;

include 'database/DataBaseConnector.php';

$dbc = new DataBaseConnector();

$host = "localhost";
$login = "root";
$pass = "root";
$dbName = "php_tests";

$dbc->startConnection($host, $login, $pass, $dbName);

echo "Disabling connection result: ". $dbc->disableConnection();