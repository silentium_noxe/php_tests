<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 14:18
 */

namespace com\bysilentium_noxe\php\tests\database;


interface InterfaceConnectorToDB
{
    public function startConnection($host, $login, $password, $dbName);
    public function disableConnection();
}