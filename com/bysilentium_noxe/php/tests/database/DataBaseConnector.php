<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 14:26
 */

namespace com\bysilentium_noxe\php\tests;

include 'InterfaceConnectorToDB.php';

use com\bysilentium_noxe\php\tests\database\InterfaceConnectorToDB;
use mysqli;

class DataBaseConnector implements InterfaceConnectorToDB
{
    private $connection = null;

    public function startConnection($host, $login, $password, $dbName)
    {

        // Create connection
        $this->connection = new mysqli($host, $login, $password, $dbName);

//        $this->connection->init();

        // Check connection
        if ($this->connection->connect_error) {
            // I got error
            // Check mysqli libs
            if (!function_exists('mysqli_init') && !extension_loaded('mysqli')) {
                echo "<p>We don't have mysqli!!!</p>";
            } else {
                echo "<p>.Phew we have it!</p>\n";
            }

            die("<p>Connection failed: " . $this->connection->connect_error . "</p>");
        }
        echo "<p>Connection successfully</p>";
    }

    public function disableConnection()
    {
        if ($this->connection == null){
            return "<p>connection is null</p>";
        }
        return $this->connection->close();
    }

    public function __toString(){
        $string = "DBC{";

        if ($this->connection){
            $string .= "connection: not_null";
        }else{
            $string .= "connection: null";
        }

        return $string .= "}";
    }

    /**
     * @return null
     */
    public function getConnection()
    {
        return $this->connection;
    }
}