<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 16:11
 */

namespace com\bysilentium_noxe\php\tests\entity;

//Entity
class Worker
{
    private static $idCounter = 0;
    private $id; //T: int

    private $name;  // T: string
    private $lastName;  // T: string
    private $patronymic;    // T: string
    private $startDate; // TODO: identify type
    private $position;  // T: string
    private $salary;    // T: int

    function __construct($name, $lastName, $patronymic, $startDate)
    {
        if (is_string($name) && is_string($lastName) && is_string($patronymic)){
            $this->id = Worker::$idCounter++;
            $this->name = $name;
            $this->lastName = $lastName;
            $this->patronymic = $patronymic;
            $this->startDate = $startDate;
        }else{
            throw new \Exception("IllegalArgument");
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getPatronymic(): string
    {
        return $this->patronymic;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

    /**
     * @param string $position
     * @throws \Exception IllegalArgument(required string)
     */
    public function setPosition($position): void
    {
        if(!is_string($position)){
            throw new \Exception("IllegalArgument");
        }
        $this->position = $position;
    }

    /**
     * @param int $salary
     * @throws \Exception IllegalArgument(required int)
     */
    public function setSalary($salary): void
    {
        if(!is_int($salary)){
            throw new \Exception("IllegalArgument");
        }
        $this->salary = $salary;
    }
}