<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 17:26
 */

namespace com\bysilentium_noxe\php\tests\entity\dao;


interface InterfaceEntityDAO
{
    function create($entity);
    function remove($entity);
    function update($entity);
    function delete($entity);
}