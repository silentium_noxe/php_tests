<?php
/**
 * Created by PhpStorm.
 * User: my_account
 * Date: 30.08.18
 * Time: 17:27
 */

namespace com\bysilentium_noxe\php\tests\entity\dao;


class WorkerDAO implements InterfaceEntityDAO
{
    private $dbConnection;

    function __construct(\mysqli $dbConnection)
    {
        $this->dbConnection = $dbConnection;

        //>>debug
        echo $this->dbConnection->info;
    }

    function create($entity)
    {
        $this->dbConnection->begin_transaction();
    }

    function remove($entity)
    {
        // TODO: Implement remove() method.
    }

    function update($entity)
    {
        // TODO: Implement update() method.
    }

    function delete($entity)
    {
        // TODO: Implement delete() method.
    }
}